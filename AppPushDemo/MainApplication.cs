﻿using System;

using Android.App;
using Android.Graphics;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Com.Alibaba.Sdk.Android.Push;
using Com.Alibaba.Sdk.Android.Push.Noonesdk;
using Com.Alibaba.Sdk.Android.Push.Register;

namespace AppPushDemo
{
    [Application]
    [MetaData("com.alibaba.app.appkey", Value = "28196201")]
    [MetaData("com.alibaba.app.appsecret", Value = "909021fd250a3e2d3aedc71777036286")]
    class MainApplication : Application
    {
        private static readonly string TAG = "Init";
        private static ICloudPushService _pushService;

        public MainApplication(IntPtr handle, JniHandleOwnership ownerShip) : base(handle, ownerShip)
        {

        }

        public override void OnCreate()
        {
            base.OnCreate();
            InitCloudChannel(this);
        }

        private void InitCloudChannel(MainApplication mainApplication)
        {
            // 安卓8.0以上必须建立通知通道
            if (Build.VERSION.SdkInt >= BuildVersionCodes.O)
            {
                var notificationManager = GetSystemService(Android.Content.Context.NotificationService) as NotificationManager;
                // 通知渠道的id
                var id = "1";
                // 用户可以看到的通知渠道的名字.
                var name = "警告任务通知";
                // 用户可以看到的通知渠道的描述
                var description = "警告任务通知";
                var importance = NotificationImportance.High;
                var mChannel = new NotificationChannel(id, name, importance)
                {
                    // 配置通知渠道的属性
                    Description = description
                };

                // 设置通知出现时的闪灯（如果 android 设备支持的话）
                mChannel.EnableLights(true);
                mChannel.LightColor = Color.Red;
                // 设置通知出现时的震动（如果 android 设备支持的话）
                mChannel.EnableVibration(true);
                mChannel.SetSound(RingtoneManager.GetDefaultUri(RingtoneType.Alarm), Notification.AudioAttributesDefault);
                mChannel.SetVibrationPattern(new long[] { 100, 200, 300, 400, 500, 400, 300, 200, 400 });
                //最后在notificationmanager中创建该通知渠道
                notificationManager.CreateNotificationChannel(mChannel);
            }

            PushServiceFactory.Init(mainApplication);
            _pushService = PushServiceFactory.CloudPushService;
            _pushService.Register(mainApplication, new CommandCallBack());
            MiPushRegister.Register(mainApplication, "2882303761518289101", "5861828976101");
        }

        class CommandCallBack : Java.Lang.Object, ICommonCallback
        {
            public void OnFailed(string errorCode, string errorMessage)
            {
                Log.Debug(TAG, "init cloudchannel failed -- errorcode:" + errorCode + " -- errorMessage:" + errorMessage);
            }

            public void OnSuccess(string p0)
            {
                Log.Debug(TAG, $"init cloudchannel success deviceId: {_pushService.DeviceId}");
            }
        }
    }
}