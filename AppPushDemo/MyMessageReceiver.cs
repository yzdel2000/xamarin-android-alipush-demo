﻿using System;
using System.Collections.Generic;

using Android.App;
using Android.Content;
using Android.Util;
using Com.Alibaba.Sdk.Android.Push;
using Com.Alibaba.Sdk.Android.Push.Notification;

namespace AppPushDemo
{
    [BroadcastReceiver(Exported = false)]
    [IntentFilter(new[] { "com.alibaba.push2.action.NOTIFICATION_OPENED", "com.alibaba.push2.action.NOTIFICATION_REMOVED", "com.alibaba.sdk.android.push.RECEIVE" })]
    public class MyMessageReceiver : MessageReceiver
    {
        protected override void OnNotification(Context context, string title, string summary, IDictionary<string, string> extraMap)
        {
            // TODO 处理推送通知
            Log.Error("MyMessageReceiver", "Receive notification, title: " + title + ", summary: " + summary + ", extraMap: " + extraMap);
            MainActivity.Adapter.AddData($"通知： title: {title}, summary: {summary}, extraMap: {extraMap}");
        }

        protected override void OnMessage(Context context, CPushMessage cPushMessage)
        {
            Log.Error("MyMessageReceiver", "onMessage, messageId: " + cPushMessage.MessageId + ", title: " + cPushMessage.Title + ", content:" + cPushMessage.Content);
            MainActivity.Adapter.AddData($"消息： id: {cPushMessage.MessageId}, title: {cPushMessage.Title}, content: {cPushMessage.Content}");
        }

        protected override void OnNotificationOpened(Context context, string title, string summary, string extraMap)
        {
            Log.Error("MyMessageReceiver", "onNotificationOpened, title: " + title + ", summary: " + summary + ", extraMap:" + extraMap);
        }

        protected override void OnNotificationClickedWithNoAction(Context context, string title, string summary, string extraMap)
        {
            Log.Error("MyMessageReceiver", "onNotificationClickedWithNoAction, title: " + title + ", summary: " + summary + ", extraMap:" + extraMap);
        }

        protected override void OnNotificationReceivedInApp(Context context, string title, string summary, IDictionary<string, string> extraMap, int openType, string openActivity, String openUrl)
        {
            Log.Error("MyMessageReceiver", "onNotificationReceivedInApp, title: " + title + ", summary: " + summary + ", extraMap:" + extraMap + ", openType:" + openType + ", openActivity:" + openActivity + ", openUrl:" + openUrl);
        }

        protected override void OnNotificationRemoved(Context context, string messageId)
        {
            Log.Error("MyMessageReceiver", "onNotificationRemoved");
        }
    }
}