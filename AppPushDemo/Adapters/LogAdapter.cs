﻿using System;

using Android.Views;
using Android.Widget;
using System.Collections.Generic;
using AndroidX.RecyclerView.Widget;

namespace AppPushDemo.Adapters
{
    public class LogAdapter : RecyclerView.Adapter
    {
        IList<string> _items;

        public LogAdapter(IList<string> data)
        {
            _items = data;
        }

        public void AddData(string data)
        {
            _items.Add(data);
            NotifyItemChanged(_items.Count);
        }

        // Create new views (invoked by the layout manager)
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            var itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.cell_log, parent, false);

            var vh = new LogAdapterViewHolder(itemView);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        public override void OnBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
        {
            var item = _items[position];

            if (viewHolder is LogAdapterViewHolder holder)
            {
                holder.TxLog.Text = item;
            }
        }

        public override int ItemCount => _items.Count;
    }

    public class LogAdapterViewHolder : RecyclerView.ViewHolder
    {
        public TextView TxLog { get; private set; }


        public LogAdapterViewHolder(View itemView) : base(itemView)
        {
            TxLog = itemView.FindViewById<TextView>(Resource.Id.txLog);
        }
    }
}