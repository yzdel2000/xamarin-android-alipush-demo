## xamarin 绑定 阿里推送整合
-------------------
* 整合阿里推送android sdk alicloud-android-push-3.1.7.
* 整合第三方推送 小米，华为.
* 附带演示项目.

### 参考项目
[AliCloudPushDroidBinding](https://github.com/haohighview/AliCloudPushDroidBinding.git)